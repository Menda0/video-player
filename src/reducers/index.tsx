
import { combineReducers } from 'redux'
import videoPlayer from './video-player'

export default combineReducers({
  videoPlayer: videoPlayer
})