import {VideoState, VideoActionTypes, Video} from "./types";
import {SET_VIDEO, LOAD_VIDEOS} from './actions';

const initialState: VideoState = {
    videoJsOptions: {
        autoplay: true,
        controls: true,
        fluid: true,
        sources: [{"type": "video/youtube", "src": ""}],
        techOrder: ["youtube"]
    },
    videos: []
};

const videoPlayer = (state: VideoState = initialState, action: VideoActionTypes) => {
    switch (action.type) {
        case SET_VIDEO:
            let stateSetVideo: VideoState = {
                "videoJsOptions":{...state.videoJsOptions},
                "videos":state.videos
            };
            let video = [{"type": "video/youtube", "src": action.video}];
            stateSetVideo.videoJsOptions.sources = video;
            return stateSetVideo;
        case LOAD_VIDEOS:
            console.log("Videos loaded", action.videos);
            let stateLoadVideos: VideoState = {
                "videoJsOptions":{...state.videoJsOptions},
                "videos":action.videos
            };
            let firstVideo = action.videos[0];
            stateLoadVideos.videoJsOptions.sources = [{"type": "video/youtube", "src": firstVideo.url}];
            return stateLoadVideos;
        default:
            return state
    }
};

export default videoPlayer