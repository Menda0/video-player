import {SET_VIDEO, LOAD_VIDEOS} from "./actions";
import videojs from 'video.js';

export interface VideoState {
    videoJsOptions: videojs.PlayerOptions,
    videos: Array<Video>
}

export interface Thumbnail {
    height: number,
    url: string,
    width: number
}

export interface Thumbnails {
    default:Thumbnail,
    high:Thumbnail,
    medium:Thumbnail
}

export interface Video{
    url: string,
    thumbnails: Thumbnails,
    name: string
}

// Video Actions
interface SetVideo {
    type: typeof SET_VIDEO,
    video: string
}

interface LoadVideos {
    type: typeof LOAD_VIDEOS,
    videos: Array<Video>
}

export type VideoActionTypes = SetVideo | LoadVideos