import {VideoActionTypes, Video} from "./types";
import CONFIG from "../../config"

export const SET_VIDEO = "SET_VIDEO";
export const LOAD_VIDEOS = "LOAD_VIDEOS";

export function setVideo(video: string): VideoActionTypes {
    return {
        type: SET_VIDEO,
        video: video
    }
}

export function loadVideos(videos: Array<Video>): VideoActionTypes {
    return {
        type: LOAD_VIDEOS,
        videos: videos
    }
}

export function fetchVideos() {
    return function (dispatch: any) {
        fetch(`${CONFIG.ENDPOINT}/videos`)
            .then(res => res.json())
            .then(
                (result:Array<Video>) => {
                    dispatch(
                        loadVideos(result)
                    )
                },
                (error) => {

                }
            )
    }
}

