import React, {useEffect} from 'react';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import Home from '@material-ui/icons/Home';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import {makeStyles} from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Link from '@material-ui/core/Link';
import VideoPlayer from './VideoPlayer';
import {setVideo, fetchVideos} from './reducers/video-player/actions';
import {Video} from './reducers/video-player/types';
import { useSelector, useDispatch } from 'react-redux'


function MadeWithLove() {
    return (
        <Typography variant="body2" color="textSecondary" align="center">
            {'Built with love by the '}
            <Link color="inherit" href="https://material-ui.com/">
                Material-UI
            </Link>
            {' team.'}
        </Typography>
    );
}

const useStyles = makeStyles(theme => ({
    icon: {
        marginRight: theme.spacing(2),
    },
    heroContent: {
        backgroundColor: theme.palette.secondary.light,
        padding: theme.spacing(8, 0, 6),
    },
    heroButtons: {
        marginTop: theme.spacing(4),
    },
    cardGrid: {
        paddingTop: theme.spacing(8),
        paddingBottom: theme.spacing(8),
    },
    card: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
        cursor:"pointer"
    },
    cardMedia: {
        paddingTop: '56.25%', // 16:9
    },
    cardContent: {
        flexGrow: 1,
    },
    footer: {
        backgroundColor: theme.palette.secondary.light,
        padding: theme.spacing(6),
    },
}));

const cards = [1, 2, 3, 4];

export default function VideoPage() {

    const classes = useStyles();

    const videoJsOptions = useSelector((state:any) => state.videoPlayer.videoJsOptions);
    const videos = useSelector((state:any) => state.videoPlayer.videos);

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchVideos())
    }, []);

    return (
        <React.Fragment>
            <CssBaseline/>
            <AppBar position="relative">
                <Toolbar>
                    <Home className={classes.icon}/>
                    <Typography variant="h6" color="inherit" noWrap>
                        This is a sample page
                    </Typography>
                </Toolbar>
            </AppBar>
            <main>
                {/* Hero unit */}
                <div className={classes.heroContent}>
                    <Container maxWidth="md">
                        <VideoPlayer />
                    </Container>
                </div>
                <Container className={classes.cardGrid} maxWidth="xl">
                    {/* End hero unit */}
                    <Grid container spacing={4}>
                        {videos.map( (video: Video) => (
                            <Grid item key={videos.url} xs={12} sm={4} md={3}>
                                <Card className={classes.card} onClick={() => dispatch(setVideo(video.url))} >
                                    <CardMedia
                                        className={classes.cardMedia}
                                        image={video.thumbnails.high.url}
                                        title={video.name}
                                    />
                                    <CardContent className={classes.cardContent}>
                                        <Typography gutterBottom variant="h5" component="h2" >
                                            {video.name}
                                        </Typography>
                                        {/*<Typography>*/}
                                        {/*    {{video.name}}*/}
                                        {/*</Typography>*/}
                                    </CardContent>
                                </Card>
                            </Grid>
                        ))}
                    </Grid>
                </Container>
            </main>
            {/* Footer */}
            <footer className={classes.footer}>
                <Typography variant="h6" align="center" gutterBottom>
                    Footer
                </Typography>
                <Typography variant="subtitle1" align="center" color="textSecondary" component="p">
                    Something here to give the footer a purpose!
                </Typography>
                <MadeWithLove/>
            </footer>
            {/* End footer */}
        </React.Fragment>
    );
}