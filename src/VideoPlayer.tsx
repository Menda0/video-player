import React, { useEffect } from 'react';
import videojs from 'video.js';

// Import Youtube
import 'videojs-youtube/dist/Youtube.min'

// Styles
import 'video.js/dist/video-js.css';
import {connect, useSelector} from "react-redux";


let player: videojs.Player;
let videoNode: HTMLVideoElement;

export default function VideoPlayer() {

    let videoJsOptions:videojs.PlayerOptions = useSelector((state:any) => state.videoPlayer.videoJsOptions);

    useEffect(() => {
        if(player){
            let source = videoJsOptions.sources![0];
            player.src({
                type: source.type,
                src: source.src,
              });
        }else{
            player = videojs(videoNode, videoJsOptions);
        }
    }, [videoJsOptions]);

    return (
        <React.Fragment>
             <div className="c-player">
                <div className="c-player__screen" data-vjs-player="true">
                    <video ref={(node: HTMLVideoElement) => videoNode = node} className="video-js"/>
                </div>
            </div>
        </React.Fragment>
    )
}