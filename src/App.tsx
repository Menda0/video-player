import React, {useReducer} from 'react';
import VideoPage from "./VideoPage";
import { createStore, applyMiddleware } from 'redux'
import { Provider  } from 'react-redux'
import thunk from 'redux-thunk';
import rootReducer from './reducers'


const store = createStore(rootReducer,applyMiddleware(thunk));

export default function App() {
    return (
        <Provider store={store}>
            <VideoPage/>
        </Provider>
    )
}
